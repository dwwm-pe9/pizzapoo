<?php

namespace Src\Controller;

use Src\Manager\PizzaManager;

class PizzaController
{

    public function getAll()
    {
        //En utilisant la Vue pizza/index.php
        $pManager = new PizzaManager();
        $pizzas = $pManager->findAll();
        include_once("Templates/pizza/index.php");
    }
}
