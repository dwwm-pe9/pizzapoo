<?php

namespace Entity;

class Pizza
{
    private int $id;
    private string $name;
    private array $ingredients;
    private string $base;
    private float $size;

    public function __construct(
        string $name = "",
        array $ingredients = [],
        string $base = "",
        float $size = 24
    ) {
        $this->name = $name;
        $this->ingredients = $ingredients;
        $this->base = $base;
        $this->size = $size;
    }
    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Get the value of base
     */
    public function getBase(): string
    {
        return $this->base;
    }

    /**
     * Set the value of base
     *
     * @return  self
     */
    public function setBase(string $base): void
    {
        $this->base = $base;
    }

    /**
     * Get the value of size
     */
    public function getSize(): float
    {
        return $this->size;
    }

    /**
     * Set the value of size
     *
     * @return  self
     */
    public function setSize(float $size): void
    {
        $this->size = $size;
    }

    /**
     * Get the value of ingredients
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    /**
     * Set the value of ingredients
     *     */
    public function setIngredients(array $ingredients): void
    {
        $this->ingredients = $ingredients;
    }


    /**
     * addIngredient
     * Alternative au setIngredients() afin d'ajouter un Ingredient sans écraser le tableau
     * @param  Ingredient $ingredient
     * @return void
     */
    public function addIngredient(Ingredient $ingredient): void
    {
        $this->ingredients[] = $ingredient;
        //array_push($this->ingredients,$ingredient);
    }

    public function removeIngredient(Ingredient $ingredient)
    {
        var_dump($ingredient);
        var_dump($this->ingredients);

        //Vérifier la prescence et recupeer l'indice 
        //de l'ingredient dans le tableau
        $indexOfIngredient = array_search($ingredient, $this->ingredients);

        //Si j'ai trouvé l'element dans le tableau
        if ($indexOfIngredient != false) {
            //Je supprime dans mon tableau a l'indice trouvé, 1 élément.
            array_splice($this->ingredients, $indexOfIngredient, 1);
        }
    }

    /**
     * Get the value of id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
