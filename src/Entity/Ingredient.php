<?php

namespace Entity;

class Ingredient
{

    private int $id;
    private string $name;
    private int $quantity;


    public function __construct(string $name = "", int $quantity = 0)
    {
        $this->name = $name;
        $this->quantity = $quantity;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Get the value of quantity
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Set the value of quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * Get the value of id
     */ 
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }
}