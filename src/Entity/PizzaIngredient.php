<?php

namespace Src\Entity;

use Entity\Ingredient;
use Entity\Pizza;

class PizzaIngredient{

    private Pizza $pizza;
    private Ingredient $ingredient;
    private int $quantity;


    /**
     * Get the value of pizza
     */ 
    public function getPizza(): Pizza
    {
        return $this->pizza;
    }

    /**
     * Set the value of pizza
     */ 
    public function setPizza(Pizza $pizza)
    {
        $this->pizza = $pizza;
    }

    /**
     * Get the value of ingredient
     */ 
    public function getIngredient(): Ingredient
    {
        return $this->ingredient;
    }

    /**
     * Set the value of ingredient
     */ 
    public function setIngredient(Ingredient $ingredient): void
    {
        $this->ingredient = $ingredient;
    }

    /**
     * Get the value of quantity
     */ 
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Set the value of quantity
     */ 
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }
}