<?php

namespace Src\Manager;

use Entity\Pizza;
use Exception;



class PizzaManager extends DatabaseManager
{
    public function findAll()
    {
        $query = $this->getConnection()->prepare("SELECT * FROM pizza");
        $query->execute([]);

        $results = $query->fetchAll();
         //TODO convertir le tableau retourné depuis la bdd, en tableaux d'objets Pizza fromArray($array)
        return $results;
    }

    public function findById(int $id): Pizza|false
    {
        $query = $this->getConnection()->prepare("SELECT * FROM pizza WHERE id = :id");
        $query->execute([":id" => $id]);

        //Verifier si j'ai un resultat
        $res = $query->fetch();


        if ($res === false) {
            return $res;
        }

        //TODO Convertir le resultat de la requete en Objet
        return  $res;
    }

    public function add(Pizza $pizza)
    {
        // private string $name;
        // private string $base;
        // private float $size;
        try {
            $response = $this->getConnection()->prepare("INSERT INTO pizza (name, base, size) VALUES (:name, :base, :size)");
            //Dupliquer la solution pour edit 

            $response->execute(
                [
                    ':name' => $pizza->getName(),
                    ':base' => $pizza->getBase(),
                    ':size' => $pizza->getSize(),
                ]
            );
        } catch (Exception $e) {
            echo ("Erreur lors de l'ajout en BDD");
            exit();
        }
    }

    public function edit(Pizza $pizza)
    {
        try {
            $response = $this->getConnection()
                ->prepare("UPDATE pizza SET name = :name, base = :base, size = :size WHERE id = :id");

            $response->execute(
                [
                    ':id' => $pizza->getId(),
                    ':name' => $pizza->getName(),
                    ':base' => $pizza->getBase(),
                    ':size' => $pizza->getSize(),
                ]
            );
        } catch (Exception $e) {
            echo($e->getMessage());
            exit();
        }
    }

    public function delete(int $id)
    {

        $query = $this->getConnection()->prepare("DELETE FROM pizza WHERE id = :id");

        $query->execute([
            ':id' => $id
        ]);
    }
}
