<?php

namespace Src\Manager;

use Entity\Ingredient;
use Exception;


class ServiceManager extends DatabaseManager
{
    public function findAll()
    {
        $query = $this->getConnection()->prepare("SELECT * FROM ingredient");
        $query->execute([]);

        $results = $query->fetchAll();

        return $results;
    }

    public function findById(int $id): Ingredient|false
    {
        $query = $this->getConnection()->prepare("SELECT * FROM ingredient WHERE id = :id");
        $query->execute([":id" => $id]);

        //Verifier si j'ai un resultat
        $res = $query->fetch();


        if ($res === false) {
            return $res;
        }

        //Convertir le resultat de la requete en Objet
        return  $res;
    }

    public function add(Ingredient $ingredient)
    {
        try {
            $response = $this->getConnection()->prepare("INSERT INTO ingredient (name, quantity) VALUES (:name, :quantity)");
            //Dupliquer la solution pour edit 

            $response->execute(
                [
                    ':name' => $ingredient->getName(),
                    ':quantity' => $ingredient->getQuantity(),
                ]
            );
        } catch (Exception $e) {
            echo ("Erreur lors de l'ajout en BDD");
            exit();
        }
    }

    public function edit(Ingredient $ingredient)
    {
        try {
            $response = $this->getConnection()
                ->prepare("UPDATE ingredient SET name = :name, quantity = :quantity WHERE id = :id");

            $response->execute(
                [
                    ':id' => $ingredient->getId(),
                    ':name' => $ingredient->getName(),
                    ':quantity' => $ingredient->getQuantity(),
                ]
            );
        } catch (Exception $e) {
            echo ($e->getMessage());
            exit();
        }
    }

    public function delete(int $id)
    {

        $query = $this->getConnection()->prepare("DELETE FROM ingredient WHERE id = :id");

        $query->execute([
            ':id' => $id
        ]);
    }
}
