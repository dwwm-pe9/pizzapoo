CREATE DATABASE IF NOT EXISTS pizzaMama;
USE pizzaMama;

CREATE TABLE Ingredient (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE Pizza (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    size FLOAT NOT NULL
) ENGINE=InnoDB;

CREATE TABLE PizzaIngredient (
    pizza_id INT,
    ingredient_id INT,
    quantity INT NOT NULL,
    PRIMARY KEY (pizza_id, ingredient_id),
    FOREIGN KEY (pizza_id) REFERENCES Pizza(id),
    FOREIGN KEY (ingredient_id) REFERENCES Ingredient(id)
) ENGINE=InnoDB;

-- Insérer des ingrédients
INSERT INTO Ingredient (name) VALUES ('Tomato'), ('Cheese'), ('Pepperoni');

-- Insérer des pizzas
INSERT INTO Pizza (name, size) VALUES ('Margherita', 12.0), ('Pepperoni', 14.0);

-- Associer des ingrédients aux pizzas avec des quantités
INSERT INTO PizzaIngredient (pizza_id, ingredient_id, quantity) VALUES
(1, 1, 2), -- Margherita avec 2 unités de Tomato
(1, 2, 1), -- Margherita avec 1 unité de Cheese
(2, 2, 3); -- Pepperoni avec 3 unités de Cheese
